<?php

namespace Apeisia\EbicsBundle\Command;

use Apeisia\EbicsBundle\Entity\EbicsBankAccount;
use Apeisia\EbicsBundle\Service\EasyEbicsFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EbicsPublicKeyCommand extends Command
{
    private EntityManagerInterface $em;
    private EasyEbicsFactory $easyEbicsFactory;

    public function __construct(EntityManagerInterface $em, EasyEbicsFactory $easyEbicsFactory)
    {
        parent::__construct();
        $this->em               = $em;
        $this->easyEbicsFactory = $easyEbicsFactory;
    }

    protected function configure()
    {
        $this->setName('ebics:public-key');
        $this->setDescription('display and update the public keys of the bank. (step 4, after activation)');
        $this->addArgument('ebicsBankAccountId', InputArgument::REQUIRED, 'The EbicsBankAccount uuid');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $bankAccountId = $input->getArgument('ebicsBankAccountId');

        /** @var EbicsBankAccount $bankAccount */
        $bankAccount = $this->em->getRepository(EbicsBankAccount::class)->find($bankAccountId);
        if (!$bankAccount) {
            $output->writeln('<error>EbicsBankAccount ' . OutputFormatter::escape($bankAccountId) . ' not found.</error>');

            return 1;
        }
        $easyEbics = $this->easyEbicsFactory->create($bankAccount);
        $easyEbics->retrieveBankPublicKeys();

        $output->writeln('Retrieved bank public keys:');
        $output->writeln('Authentication (X):');
        $output->writeln($easyEbics->getKeyRing()->getBankSignatureX()->getPublicKey());
        $output->writeln('Encryption (E):');
        $output->writeln($easyEbics->getKeyRing()->getBankSignatureE()->getPublicKey());

        $this->em->flush();

        return 0;
    }
}
