<?php

namespace Apeisia\EbicsBundle\Command;

use AndrewSvirin\Ebics\Exceptions\EbicsException;
use AndrewSvirin\Ebics\Exceptions\NoDownloadDataAvailableException;
use Apeisia\EbicsBundle\Entity\EbicsBankAccount;
use Apeisia\EbicsBundle\Event\EbicsTransactionReceivedEvent;
use Apeisia\EbicsBundle\Service\EasyEbicsFactory;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Genkgo\Camt\DTO\Record;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class EbicsImportCommand extends Command
{
    private EntityManagerInterface $em;
    private LoggerInterface $logger;
    private EasyEbicsFactory $easyEbicsFactory;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        EntityManagerInterface   $em,
        LoggerInterface          $logger,
        EasyEbicsFactory         $easyEbicsFactory,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();
        $this->em               = $em;
        $this->logger           = $logger;
        $this->easyEbicsFactory = $easyEbicsFactory;
        $this->eventDispatcher  = $eventDispatcher;
    }

    protected function configure()
    {
        $this->setName('ebics:import');
        $this->setDescription('Run transactions import from all configured bank accounts');
        $this->addOption('ignore-last-sync', null, InputOption::VALUE_NONE, 'do not set LastSync to NOW');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $accounts          = $this->em->getRepository(EbicsBankAccount::class)->findBy([
            'enabled' => true,
        ]);
        $accountStatements = [];

        /** @var EbicsBankAccount $bankAccount */
        foreach ($accounts as $bankAccount) {
            if (!$this->easyEbicsFactory->hasCredentials($bankAccount)) {
                $this->logger->error('No credentials found for account ' . $bankAccount->getId());
                continue;
            }
            // only sync the ones that do not use the data from another one
            if (!$this->easyEbicsFactory->getCredential($bankAccount, 'useFrom')) {
                $this->logger->info('Syncing ' . $bankAccount->getId());
                try {
                    $statements = $this->pullStatements($bankAccount);
                    $this->logger->info('Pulled ' . count($statements) . ' statements');
                    $this->syncAccount($bankAccount, $statements);
                    if(!$input->getOption('ignore-last-sync')) {
                        $bankAccount->setLastSync(new DateTime());
                    }
                    $this->em->flush();
                    $accountStatements[$bankAccount->getId()] = $statements;
                } catch (EbicsException $e) {
                    $this->logger->error('Ebics exception for ' . $bankAccount->getId() . ': ' . $e->getMessage());
                    $accountStatements[$bankAccount->getId()] = [];
                }
            }
        }
        /** @var EbicsBankAccount $account */
        foreach ($accounts as $bankAccount) {
            // now the other way around
            $useFrom = $this->easyEbicsFactory->getCredential($bankAccount, 'useFrom');
            if ($useFrom) {
                $cnt = count($accountStatements[$useFrom]);
                if ($cnt > 0) {
                    $this->logger->info('Syncing ' . $bankAccount->getId() . ' using ' . $cnt . ' statements from ' . $useFrom);
                    $this->syncAccount($bankAccount, $accountStatements[$useFrom]);
                    if(!$input->getOption('ignore-last-sync')) {
                        $bankAccount->setLastSync(new DateTime());
                    }
                    $this->em->flush();
                } else {
                    $this->logger->info('Skipping ' . $bankAccount->getId() . ' because ' . $useFrom . ' has no statements');
                }
            }
        }
        return 0;
    }

    private function pullStatements(EbicsBankAccount $bankAccount): ?array
    {

        $ebics = $this->easyEbicsFactory->create($bankAccount);

        $lastSync = $bankAccount->getLastSync();
        $from     = (new DateTime('-1 month'));

        if ($lastSync !== null && $lastSync >= $from) {
            $from = $lastSync;
        }

        $this->logger->info('Fetching transactions since ' . $from->format('Y-m-d') . ' for ' . $bankAccount->getId() . '.');

        $from = $from->setTime(0, 0, 0);
        try {
            return $ebics->getTransactions(new DateTime(), $from, new DateTime());
        } catch (NoDownloadDataAvailableException $e) {
            return [];
        }
    }

    /**
     * @param EbicsBankAccount $bankAccount
     * @param Record[] $records
     * @throws Exception
     */
    private function syncAccount(EbicsBankAccount $bankAccount, array $records)
    {
        $filterAccount = $this->easyEbicsFactory->getCredential($bankAccount, 'filterAccount');

        foreach ($records as $record) {
            if ($filterAccount) {
                if ($record->getAccount()->getIdentification() != $filterAccount) {
                    continue;
                }
            }
            $this->logger->info(
                'Processing records from account '.$record->getAccount()->getIdentification()
            );

            foreach ($record->getEntries() as $transaction) {

                $ebicsTransaction = new EbicsTransactionReceivedEvent($bankAccount, $transaction);
                $this->eventDispatcher->dispatch($ebicsTransaction);


                $this->logger->info('Transaction', $ebicsTransaction->toArray());
            }
        }
    }
}
