<?php

namespace Apeisia\EbicsBundle\Command;

use Apeisia\EbicsBundle\Entity\EbicsBankAccount;
use Apeisia\EbicsBundle\Service\EasyEbicsFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EbicsInitializeCommand extends Command
{
    private EntityManagerInterface $em;
    private EasyEbicsFactory $easyEbicsFactory;

    public function __construct(EntityManagerInterface $em, EasyEbicsFactory $easyEbicsFactory)
    {
        parent::__construct();
        $this->em               = $em;
        $this->easyEbicsFactory = $easyEbicsFactory;
    }

    protected function configure()
    {
        $this->setName('ebics:initialize');
        $this->setDescription('create certificates and send them to the bank. (step 2)');
        $this->addArgument('ebicsBankAccountId', InputArgument::REQUIRED, 'The EbicsBankAccount uuid');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $bankAccountId = $input->getArgument('ebicsBankAccountId');

        /** @var EbicsBankAccount $bankAccount */
        $bankAccount = $this->em->getRepository(EbicsBankAccount::class)->find($bankAccountId);
        if (!$bankAccount) {
            $output->writeln('<error>EbicsBankAccount ' . OutputFormatter::escape($bankAccountId) . ' not found.</error>');

            return 1;
        }

        if ($bankAccount->getKeyring() === null) {
            $output->writeln('<error>We expect that keyring is already an empty array</error>');

            return 1;
        }

        if (count($bankAccount->getKeyring()) > 0) {
            $output->writeln('<error>This operation cannot be executed on an EbicsBankAccount that has keys.</error>');

            return 1;
        }

        $easyEbics = $this->easyEbicsFactory->create($bankAccount);
        $easyEbics->generateKeysAndPushToBank();
        $this->em->flush();

        $output->writeln('Bank initialization done. run "ebics:letter ' . OutputFormatter::escape($bankAccount->getId()) . '" now to get the initialization letter.');

        return 0;
    }
}
