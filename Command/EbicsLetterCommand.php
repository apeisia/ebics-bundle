<?php

namespace Apeisia\EbicsBundle\Command;

use AndrewSvirin\Ebics\Models\SignatureBankLetter;
use Apeisia\EbicsBundle\Entity\EbicsBankAccount;
use Apeisia\EbicsBundle\Service\EasyEbicsFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EbicsLetterCommand extends Command
{
    private EntityManagerInterface $em;
    private EasyEbicsFactory $easyEbicsFactory;

    public function __construct(EntityManagerInterface $em, EasyEbicsFactory $easyEbicsFactory)
    {
        parent::__construct();
        $this->em               = $em;
        $this->easyEbicsFactory = $easyEbicsFactory;
    }

    protected function configure()
    {
        $this->setName('ebics:letter');
        $this->setDescription('print a letter to send to the bank.  (step 3)');
        $this->addArgument('ebicsBankAccountId', InputArgument::REQUIRED, 'The EbicsBankAccount uuid');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $bankAccountId = $input->getArgument('ebicsBankAccountId');

        /** @var EbicsBankAccount $bankAccount */
        $bankAccount = $this->em->getRepository(EbicsBankAccount::class)->find($bankAccountId);
        if (!$bankAccount) {
            $output->writeln('<error>EbicsBankAccount ' . OutputFormatter::escape($bankAccountId) . ' not found.</error>');

            return 1;
        }

        if (count($bankAccount->getKeyring()) == 0) {
            $output->writeln('<error>This operation can only be executed on an EbicsBankAccount that has keys.</error>');

            return 1;
        }

        $easyEbics = $this->easyEbicsFactory->create($bankAccount);
        $letter    = $easyEbics->createInitializationLetter();

        $keys = [
            'A' => $letter->getSignatureBankLetterA(),
            'E' => $letter->getSignatureBankLetterE(),
            'X' => $letter->getSignatureBankLetterX(),
        ];
        /**
         * @var string $indicator
         * @var SignatureBankLetter $key
         */
        foreach ($keys as $indicator => $key) {
            $mod = chunk_split($key->getModulus(), 48);
            $hash = chunk_split($key->getKeyHash(), 48);
            $output->write("\nÖffentlicher Schlüssel $indicator:\n" .
                "Exponent: {$key->getExponent()}\n" .
                "Modulo:\n{$mod}\n" .
                "Hash:\n{$hash}\n\n\n");
        }

        return 0;
    }
}
