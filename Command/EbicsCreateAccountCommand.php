<?php

namespace Apeisia\EbicsBundle\Command;

use Apeisia\EbicsBundle\Entity\EbicsBankAccount;
use Apeisia\LoginAccess\Service\RelationConfiguration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class EbicsCreateAccountCommand extends Command
{
    private EntityManagerInterface $em;
    private RelationConfiguration $relationConfiguration;

    public function __construct(EntityManagerInterface $em, RelationConfiguration $relationConfiguration)
    {
        parent::__construct();
        $this->em = $em;
        $this->relationConfiguration = $relationConfiguration;
    }

    protected function configure()
    {
        $this->setName('ebics:create-account');
        $this->setDescription('Create a new bank account (step 1).');
        $this->addArgument('iban', InputArgument::REQUIRED, 'Account IBAN');
        $this->addArgument('bic', InputArgument::REQUIRED, 'Account BIC');
        $this->addArgument('owner', InputArgument::REQUIRED, 'Account owner (don\'t forget to quote input)');
        $this->addOption('account', null, InputOption::VALUE_REQUIRED, 'The account ID this bank account belongs to (LoginAccess Account)');
        $this->addOption('entity', null, InputOption::VALUE_REQUIRED, 'Entity, if not ' . EbicsBankAccount::class, EbicsBankAccount::class);
        $this->addOption('name', null, InputOption::VALUE_REQUIRED, 'Readable name for this bank account');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entity = $input->getOption('entity');

        /** @var EbicsBankAccount $bankAccount */
        $bankAccount = new $entity();
        $bankAccount->setIban($input->getArgument('iban'));
        $bankAccount->setBic($input->getArgument('bic'));
        $bankAccount->setAccountOwner($input->getArgument('owner'));

        if ($input->getOption('account')) {
            $account = $this->em->getRepository($this->relationConfiguration->accountClass)->find($input->getOption('account'));
            $bankAccount->setAccount($account);
        }

        if ($input->getOption('name')) {
            $bankAccount->setName($input->getOption('name'));
        }

        $this->em->persist($bankAccount);
        $this->em->flush();

        $output->writeln('Bank account created. Don\'t forget to add credentials to parameters.yaml. The ID is: ' . OutputFormatter::escape($bankAccount->getId()));
        $output->writeln('After that, run "ebics:initialize ' . OutputFormatter::escape($bankAccount->getId()) . '" to create certificates and send them to the bank.');

        return 0;
    }
}
