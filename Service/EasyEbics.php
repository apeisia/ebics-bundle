<?php

namespace Apeisia\EbicsBundle\Service;

use AndrewSvirin\Ebics\Contracts\KeyRingManagerInterface;
use AndrewSvirin\Ebics\EbicsBankLetter;
use AndrewSvirin\Ebics\EbicsClient;
use AndrewSvirin\Ebics\Exceptions\EbicsException;
use AndrewSvirin\Ebics\Handlers\ResponseHandlerV2;
use AndrewSvirin\Ebics\Models\Bank;
use AndrewSvirin\Ebics\Models\BankLetter;
use AndrewSvirin\Ebics\Models\KeyRing;
use AndrewSvirin\Ebics\Models\User;
use DateTime;
use Genkgo\Camt\Config;
use Genkgo\Camt\DTO\Entry;
use Genkgo\Camt\Reader;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class EasyEbics
{
    public KeyRing     $keyRing;
    public EbicsClient $client;

    public function __construct(
        private KeyRingManagerInterface $keyRingManager,
        Bank                            $bank,
        User                            $user
    )
    {
        $this->keyRingManager = $keyRingManager;
        $this->loadKeyRing();
        $this->client = new EbicsClient($bank, $user, $this->keyRing);

    }

    private function loadKeyRing()
    {
        // don't worry about the empty strings, the password is passed in the constructor
        $this->keyRing = $this->keyRingManager->loadKeyRing('', '');
    }

    private function saveKeyRing()
    {
        // don't worry about the empty strings, the password is passed in the constructor
        $none = '';
        $this->keyRingManager->saveKeyRing($this->keyRing, $none);
    }

    public function getKeyRing()
    {
        return $this->keyRing;
    }

    /**
     * generate new keys, send the new keys to the bank, retrieve the public
     * keys of the bank and save everything in the keyring
     *
     * @throws EbicsException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function generateKeysAndPushToBank()
    {
        // Send public certificate of signature A006 to the bank
        $this->client->INI(); // INI generates new keys
        // Send public certificates of authentication (X002) and encryption (E002) to the bank
        $this->client->HIA(); // HIA also generates new keys

        $this->saveKeyRing();
    }

    /**
     * @throws ClientExceptionInterface
     * @throws EbicsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function retrieveBankPublicKeys()
    {
        // Retrieve the Bank public certificates authentication (X002) and encryption (E002)
        $this->client->HPB();
        $this->saveKeyRing();
    }

    /**
     * create a instance of InitializationLetter to get all the information that you need to send an
     * initialization letter to your bank
     *
     * @return BankLetter
     */
    public function createInitializationLetter()
    {
        $ebicsBankLetter = new EbicsBankLetter();

        return $ebicsBankLetter->prepareBankLetter(
            $this->client->getBank(),
            $this->client->getUser(),
            $this->client->getKeyring()
        );

        //return $ebicsBankLetter->formatBankLetter($bankLetter, $ebicsBankLetter->createHtmlBankLetterFormatter());
    }

    /**
     * Get the parsed transactions
     *
     * @param DateTime|null $dateTime
     * @param DateTime|null $startDateTime
     * @param DateTime|null $endDateTime
     * @return Entry[]|null
     * @throws ClientExceptionInterface
     * @throws EbicsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function getTransactions(DateTime $dateTime = null,
                                    DateTime $startDateTime = null,
                                    DateTime $endDateTime = null)
    {
        $sta = $this->client->C53($dateTime, $startDateTime, $endDateTime);
        if (!$sta)
            return null;

        $allRecords = [];
        $offset     = 0;
        $config = Config::getDefault();
        $config->disableXsdValidation();

        foreach ($sta->getTransaction()->getSegments() as $segment) {
            $data       = $segment->getOrderData();
            // $data contains multiple XML documents concatenated with some data (including pk-zipped) between them.
            // we don't know the correct format of the data, so we have to search for the XML documents
            while (true) {
                // find beginning of XML document
                $pos1 = strpos($data, '<?', $offset);
                if ($pos1 === false) {
                    break;
                }
                // find end of XML document (next part is always PK)
                $pos2    = strpos($data, '</Document>PK', $offset);
                $xml     = substr($data, $pos1, $pos2 - $pos1 + 11);

                // add 13 to $pos2 to skip the </Document>PK part
                $offset  = $pos2 + 13;
                $reader  = new Reader($config);
                $message = $reader->readString($xml);
                foreach ($message->getRecords() as $record) {
                    $allRecords[] = $record;
                }
            }
        }
        return $allRecords;
    }
}
