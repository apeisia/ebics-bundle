<?php

namespace Apeisia\EbicsBundle\Service;

use AndrewSvirin\Ebics\Models\Bank;
use AndrewSvirin\Ebics\Models\User;
use Apeisia\EbicsBundle\Entity\EbicsBankAccount;
use Doctrine\ORM\EntityManagerInterface;

class EasyEbicsFactory
{
    private array $credentials;

    public function __construct(array $credentials, private readonly EntityManagerInterface $em)
    {
        $this->credentials = $credentials;
    }

    public function hasCredentials(EbicsBankAccount $account): bool
    {
        return array_key_exists($account->getId(), $this->credentials);
    }

    public function getCredential(EbicsBankAccount $account, $key)
    {
        $a = $this->credentials[$account->getId()];
        if (array_key_exists($key, $a)) {
            return $a[$key];
        }
        return null;
    }

    public function create(EbicsBankAccount $account): EasyEbics
    {
        $credentials = $this->credentials[$account->getId()];
        $bank = new Bank($credentials['hostId'], $credentials['hostUrl']);
        $bank->setIsCertified(false);
        $bank->setServerName('');

        return new EasyEbics(
            new KeyRingManager($account, $credentials['keyPassword'], $this->em),
            $bank,
            new User($credentials['partnerId'], $credentials['userId'])
        );
    }
}
