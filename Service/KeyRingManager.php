<?php

namespace Apeisia\EbicsBundle\Service;

use AndrewSvirin\Ebics\Contracts\KeyRingManagerInterface;
use AndrewSvirin\Ebics\Factories\KeyRingFactory;
use AndrewSvirin\Ebics\Models\KeyRing;
use Apeisia\EbicsBundle\Entity\EbicsBankAccount;
use Doctrine\ORM\EntityManagerInterface;

class KeyRingManager implements KeyRingManagerInterface
{
    private EbicsBankAccount $bankAccount;
    private string $password;

    public function __construct(EbicsBankAccount $bankAccount, string $password, private readonly EntityManagerInterface $em)
    {
        $this->bankAccount = $bankAccount;
        $this->password    = $password;
    }

    /**
     * @inheritDoc
     */
    function loadKeyRing($resource, string $passphrase): KeyRing
    {
        if (count($this->bankAccount->getKeyring())) {
            $factory = new KeyRingFactory();
            $result  = $factory->createKeyRingFromData($this->bankAccount->getKeyring());
        } else {
            $result = new KeyRing();
        }
        $result->setPassword($this->password);

        return $result;
    }

    /**
     * @inheritDoc
     */
    function saveKeyRing(KeyRing $keyRing, &$resource): void
    {
        $factory = new KeyRingFactory();
        $data    = $factory->buildDataFromKeyRing($keyRing);
        var_dump($data);
        $this->bankAccount->setKeyring($data);
        $this->em->flush();
    }
}
