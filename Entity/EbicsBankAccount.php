<?php

namespace Apeisia\EbicsBundle\Entity;

use Apeisia\BaseBundle\Entity\EntityUUId;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 */
#[ORM\Entity]
class EbicsBankAccount
{
    use EntityUUId;

    /**
     * @ORM\Column(type="json", nullable=false)
     * @Serializer\Exclude()
     */
    #[ORM\Column(type: 'json', nullable: false)]
    #[Serializer\Exclude]
    private ?array $keyring = [];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Exclude()
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Serializer\Exclude]
    private ?DateTime $lastSync = null;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=1})
     */
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $enabled = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $accountOwner = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Groups({"select", "EbicsBankAccount.iban"})
     */
    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $iban = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Groups({"EbicsBankAccount.bic"})
     */
    #[ORM\Column(type: 'string', nullable: true)]
    #[Serializer\Groups(['EbicsBankAccount.bic'])]
    private ?string $bic = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Groups({"select", "EbicsBankAccount.name"})
     */
    #[ORM\Column(type: 'string', nullable: true)]
    #[Serializer\Groups(['select', 'EbicsBankAccount.name'])]
    private ?string $name = null;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=1})
     */
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 1])]
    private bool $defaultForAccount = true;

    /**
     * @var mixed
     */
    private $account = null;


    public function getKeyring(): ?array
    {
        return $this->keyring;
    }

    public function setKeyring(?array $keyring): self
    {
        $this->keyring = $keyring;
        return $this;
    }

    public function getLastSync(): ?DateTime
    {
        return $this->lastSync;
    }

    public function setLastSync(?DateTime $lastSync): self
    {
        $this->lastSync = $lastSync;
        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getAccountOwner(): ?string
    {
        return $this->accountOwner;
    }

    public function setAccountOwner(?string $accountOwner): self
    {
        $this->accountOwner = $accountOwner;
        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;
        return $this;
    }

    function getBic(): ?string
    {
        return $this->bic;
    }

    public function setBic(?string $bic): self
    {
        $this->bic = $bic;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount($account): self
    {
        $this->account = $account;
        return $this;
    }


}
