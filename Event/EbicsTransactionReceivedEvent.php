<?php

namespace Apeisia\EbicsBundle\Event;

use Apeisia\EbicsBundle\Entity\EbicsBankAccount;
use Genkgo\Camt\DTO\Entry;
use Money\Money;

class EbicsTransactionReceivedEvent
{

    const DEBIT = 'D';
    const CREDIT = 'C';

    private EbicsBankAccount $ebicsBankAccount;
    private \DateTimeImmutable $valueDate;
    private \DateTimeImmutable $bookingDate;
    private Money $amount;
    private string $transactionId;
    private string $additionalInfo;
    private string $status;
    private string $name;
    private string $iban;
    private string $description;
    private string $hash;
    private Entry $transaction;


    public function __construct(EbicsBankAccount $account, Entry $transaction)
    {
        $this->ebicsBankAccount = $account;
        $this->transaction      = $transaction;
        $detail                 = $transaction->getTransactionDetail();


        $this->bookingDate    = $transaction->getBookingDate();
        $this->valueDate      = $transaction->getValueDate();
        $this->amount         = $transaction->getAmount();
        $this->transactionId  = $detail->getReference()->getTransactionId();
        $this->additionalInfo = $transaction->getAdditionalInfo();
        $this->status         = $transaction->getStatus();
        if($detail->getRemittanceInformation()) {
            $this->description = $detail->getRemittanceInformation()->getUnstructuredBlock()->getMessage();
        } else {
            $this->description = '[not provided by bank]';
        }

        $this->name = '';
        $this->iban = '';

        // In some transaction like the monthly bank fee relatedParty is not set
        if (count($detail->getRelatedParties()) == 1) {
            $this->name = $detail->getRelatedParty()->getRelatedPartyType()->getName();
            $this->iban = $detail->getRelatedParty()->getAccount()->getIdentification();
        }
        else if (count($detail->getRelatedParties()) > 1) {
            $foundParty = $detail->getRelatedParties()[0]; // fallback
            foreach($detail->getRelatedParties() as $relatedParty) {
                if($relatedParty->getAccount() != null && $relatedParty->getAccount()->getIdentification() && $relatedParty->getAccount()->getIdentification() != $account->getIban()) {
                    $foundParty = $relatedParty;
                    break;
                }
            }
            if($foundParty->getRelatedPartyType() != null) {
                $this->name = $foundParty->getRelatedPartyType()->getName();
            }
            if($foundParty->getAccount() != null) {
                $this->iban = $foundParty->getAccount()->getIdentification();
            }
        }



        $this->updateHash();


    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getEbicsBankAccount(): EbicsBankAccount
    {
        return $this->ebicsBankAccount;
    }

    public function getValueDate(): ?\DateTimeImmutable
    {
        return $this->valueDate;
    }

    public function getBookingDate(): ?\DateTimeImmutable
    {
        return $this->bookingDate;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function getAdditionalInfo(): ?string
    {
        return $this->additionalInfo;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getName(): ?string
    {
        return $this->name ?? null;
    }

    public function getIban(): ?string
    {
        return $this->iban ?? null;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getTransaction(): Entry
    {
        return $this->transaction;
    }


    public function updateHash()
    {
        $this->hash = sha1(join('',$this->toArray()));
    }

    public function toArray(): array
    {
        return [
            'name' =>  $this->getName(),
            'status' =>  $this->getStatus(),
            'additionalInfo' =>  $this->getAdditionalInfo(),
            'valueDate' =>  $this->getValueDate()->format('Y-m-d H:i:s'),
            'bookingDate' =>  $this->getBookingDate()->format('Y-m-d H:i:s'),
            'amount' =>  $this->getAmount()->getAmount(),
            'transactionId' =>  $this->getTransactionId(),
            'iban' =>  $this->getIban(),
            'description' =>  $this->getDescription(),
        ];
    }

    private function anonymizeAllButLastCharacters(string $string, int $keepLetters = 4): string
    {
        return strlen($string) <= $keepLetters ? $string : str_repeat('*', strlen($string) - 4) . substr($string, -$keepLetters);
    }
}
